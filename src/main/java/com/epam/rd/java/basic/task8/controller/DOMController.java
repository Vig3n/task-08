package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import com.epam.rd.java.basic.task8.util.Transformer;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public Flowers getFlowers() {
		return flowers;
	}

	// PLACE YOUR CODE HERE
	public void DOMparse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		System.out.println("DocumentBuilderFactory ==> " + dbf.getClass());
		dbf.setNamespaceAware(true);
		if(validate){
			dbf.setFeature("http://xml.org/sax/features/validation" ,true);
			dbf.setFeature("http://apache.org/xml/features/validation/schema", true);
		}
		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				// throw exception if XML document is NOT valid
				throw e;
			}
		});

		Document document = db.parse(xmlFileName);
		Element root = document.getDocumentElement();
		flowers = new Flowers();
		NodeList nList = document.getElementsByTagName("flower");
		for (int i = 0; i < nList.getLength(); i++) {
			Flower flower = getFlower(nList.item(i));
			flowers.getFlower().add(flower);
		}
	}


	public void DOMValidate(){
		DocumentBuilder parser = null;
		try {
			parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		Document document = null;
		try {
			 document = parser.parse(new File(xmlFileName));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		Source schemaFile = new StreamSource(new File("input.xsd"));
		Schema schema = null;
		try {
			 schema = factory.newSchema(schemaFile);
		} catch (SAXException e) {
			e.printStackTrace();
		}
		Validator validator = schema.newValidator();
		try {
			validator.validate(new DOMSource(document));
		} catch (SAXException | IOException e) {
			e.printStackTrace();
		}
	}

//	public static  class DOMSortingComparator implements Comparator<Flower>{
////
////		@Override
////		public int compare(Flower o1, Flower o2) {
////			int NameCompare = o1.getName().compareTo(o2.getName());
////			return NameCompare == 0 ? 0 : 1;
////		}
////	}

	public static Comparator<Flower> compByName = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getName().compareToIgnoreCase(o2.getName());
		}

	};

//	public static void sortByName(List<Flower> flowers){
//		flowers.sort(compByName);
//	}

	public static final void sortByName(Flowers flowers) {
		Collections.sort(flowers.getFlower(), compByName);
	}

	private Flower getFlower(Node node){
		Flower flower = new Flower();
		Element flElement = (Element) node;
		Node nameNode = flElement.getElementsByTagName("name").item(0);
		flower.setName(nameNode.getTextContent());

		Node soilNode = flElement.getElementsByTagName("soil").item(0);
		flower.setSoil(soilNode.getTextContent());

		Node originNode = flElement.getElementsByTagName("origin").item(0);
		flower.setOrigin(originNode.getTextContent());

		Node multiNode = flElement.getElementsByTagName("multiplying").item(0);
		flower.setMultiplying(multiNode.getTextContent());

		NodeList visualNodeList = flElement.getElementsByTagName("visualParameters");
		for(int i = 0; i < visualNodeList.getLength(); i++){
			VisualParameters vp = getVisualParameters(visualNodeList.item(i));
			flower.getVp().add(vp);
		}
		NodeList gTNodeList = flElement.getElementsByTagName("growingTips");
		for(int i = 0; i < gTNodeList.getLength(); i++){
			GrowingTips gt = getGT(gTNodeList.item(i));
			flower.getGt().add(gt);
		}
		return flower;
	}

	private VisualParameters getVisualParameters(Node node){
		VisualParameters vp = new VisualParameters();
		Element vpElement = (Element) node;

		String stColour = vpElement.getElementsByTagName("stemColour").item(0).getTextContent();
		String lfColour = vpElement.getElementsByTagName("leafColour").item(0).getTextContent();
		int flLen =Integer.parseInt(vpElement.getElementsByTagName("aveLenFlower").item(0).getTextContent());
		vp.setLeafColour(lfColour);
		vp.setStemColour(stColour);
		vp.setAveLen(flLen);
		return vp;
	}

	private GrowingTips getGT(Node node){
		GrowingTips gt = new GrowingTips();
		Element gtElement = (Element) node;
		String light = gtElement.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent();
		gt.setLighing(String.valueOf(light));
		String temp = gtElement.getElementsByTagName("tempreture").item(0).getTextContent();
		gt.setTempreture(Integer.parseInt(temp));
		String watering = gtElement.getElementsByTagName("watering").item(0).getTextContent();
		gt.setMeasure(Integer.parseInt(watering));
		return gt;
	}

	public static Document getDocument(Flowers flowers, String tnsPrefix)
			throws ParserConfigurationException {

		if (tnsPrefix == null || tnsPrefix.isEmpty()) {
			throw new IllegalArgumentException(
					"Target namespace prefix cannot be null or empty");
		}

		// get document builder factory
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		// set properties for Factory
		dbf.setNamespaceAware(true); // <-- XML document has namespace

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();

		// create qualified name for root element
		String qualifiedName = "flowers";

		// create root element

		Element sElement = document.createElementNS("http://www.nure.ua",
				qualifiedName);

		// set schema location
		sElement.setAttributeNS(XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
				"xsi:schemaLocation",
				"http://www.nure.ua input.xsd ");

		document.appendChild(sElement);

		// add questions elements
		for (Flower flower : flowers.getFlower()) {
			// add question
			Element fElement = document.createElement("flower");
			sElement.appendChild(fElement);

			// add question text
			Element nameElement = document.createElement("name");
			nameElement.setTextContent(flower.getName());
			fElement.appendChild(nameElement);

			Element soilElement = document.createElement("soil");
			soilElement.setTextContent(flower.getSoil());
			fElement.appendChild(soilElement);

			Element originElement = document.createElement("origin");
			originElement.setTextContent(flower.getOrigin());
			fElement.appendChild(originElement);

			Element vpElement = document.createElement("visualParameters");
			fElement.appendChild(vpElement);
			// add visual parameters
			for (VisualParameters visualParameters : flower.getVp()) {
				Element stemColourElement = document.createElement("stemColour");
				stemColourElement.setTextContent(visualParameters.getStemColour());
				vpElement.appendChild(stemColourElement);

				Element leafColourElement = document.createElement("leafColour");
				leafColourElement.setTextContent(visualParameters.getLeafColour());
				vpElement.appendChild(leafColourElement);

				Element aveLenElement = document.createElement("aveLenFlower");
				vpElement.appendChild(aveLenElement);
				Attr attr = document.createAttribute("measure");
				attr.setValue("cm");
				aveLenElement.setAttributeNode(attr);
				aveLenElement.setTextContent(String.valueOf(visualParameters.getAveLen()));
			}

			Element gtElement = document.createElement("growingTips");
			fElement.appendChild(gtElement);
			// add growing tips
			for(GrowingTips growingTips : flower.getGt()){
				Element tempElement = document.createElement("tempreture");
				gtElement.appendChild(tempElement);
				Attr attr = document.createAttribute("measure");
				attr.setValue("celcius");
				tempElement.setAttributeNode(attr);
				tempElement.setTextContent(String.valueOf(growingTips.getTempreture()));

				Element lightingElement = document.createElement("lighting");
				gtElement.appendChild(lightingElement);
				attr = document.createAttribute("lightRequiring");
				attr.setValue(growingTips.getLighing());
				lightingElement.setAttributeNode(attr);


				Element wateringElement = document.createElement("watering");
				gtElement.appendChild(wateringElement);
				attr = document.createAttribute("measure");
				attr.setValue("mlPerWeek");
				wateringElement.setAttributeNode(attr);
				wateringElement.setTextContent(String.valueOf(growingTips.getMeasure()));
			}
			Element multiElement = document.createElement("multiplying");
			multiElement.setTextContent(flower.getMultiplying());
			fElement.appendChild(multiElement);
		}
		return document;
	}

	public static void saveToXML(Flowers flowers, String xmlFileName, String tnsPrefix)
			throws TransformerException, ParserConfigurationException {
		Transformer.saveToXML(getDocument(flowers, tnsPrefix), xmlFileName); 	// XML
	}





}
