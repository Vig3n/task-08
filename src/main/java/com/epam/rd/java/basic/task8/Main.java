package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;

public class Main {

	public static void usage() {
		System.out.println(" com.epam.rd.java.basic.task8 input.xml");
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			usage();
			System.out.println(args[0]);
			return;
		}

		
		String xmlFileName = args[0];
		String xsdFileName = args[1]; // <-- this parameter for JAXBController
		String xslFileName = null;
		if (args.length == 3) {
			xslFileName = args[2];
		}
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////

		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.DOMparse(true);
		Flowers flowers = domController.getFlowers();
		// PLACE YOUR CODE HERE

		// sort (case 1)
		domController.sortByName(flowers);

		// save container to XML
		String outputXmlFileName = "output.dom.xml";
		// PLACE YOUR CODE HERE

		// save
		DOMController.saveToXML(flowers, outputXmlFileName, "dom-prefix");
		// PLACE YOUR CODE HERE

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
//		// PLACE YOUR CODE HERE
		saxController.parse(true);
		flowers = saxController.getFlowers();
		String outputSaxFileName = "output.sax.xml";
//		// sort  (case 2)
//		// PLACE YOUR CODE HERE
		SAXController.sortByTemp(flowers);
		DOMController.saveToXML(flowers, outputSaxFileName,"dom-prefix");
//		// PLACE YOUR CODE HERE
//
//		////////////////////////////////////////////////////////
//		// StAX
//		////////////////////////////////////////////////////////
//
//		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		staxController.parse();
		flowers = staxController.getFlowers();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		STAXController.sortByTemp(flowers);
		DOMController.saveToXML(flowers, "output.stax.xml", "dom-prefix");
		// PLACE YOUR CODE HERE
	}

}
