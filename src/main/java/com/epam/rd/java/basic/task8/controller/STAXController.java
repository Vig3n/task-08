package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.GrowingTips;
import com.epam.rd.java.basic.task8.entity.VisualParameters;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.*;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flowers flowers;

	public Flowers getFlowers(){
		return flowers;
	}

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public void parse() throws XMLStreamException {
		Flower flower = null;
		GrowingTips growingTips = null;
		VisualParameters visualParameters = null;
		String currentElement = null;

		XMLInputFactory factory = XMLInputFactory.newInstance();

		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while(reader.hasNext()){
			XMLEvent event = reader.nextEvent();

			if(event.isCharacters() && event.asCharacters().isWhiteSpace())
				continue;

			if(event.isStartElement()){
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();

				if(currentElement.equals("flowers")){
					flowers = new Flowers();
					continue;
				}

				if(currentElement.equals("flower")){
					flower = new Flower();
					continue;
				}

				if(currentElement.equals("visualParameters")){
					visualParameters = new VisualParameters();
					Attribute attribute = startElement.getAttributeByName(new QName("aveLenFlower"));
					if(attribute != null)
						visualParameters.setLenAttr(attribute.getValue());
					continue;
				}

				if(currentElement.equals("growingTips")){
					growingTips = new GrowingTips();
					Attribute attribute = startElement.getAttributeByName(new QName("tempreture"));
					if(attribute != null)
						growingTips.setAttrTemp(attribute.getValue());
					attribute = startElement.getAttributeByName(new QName("lighting"));
					if(attribute != null)
						growingTips.setTempreture(Integer.parseInt(attribute.getValue()));
					attribute = startElement.getAttributeByName(new QName("watering"));
					if(attribute != null)
						growingTips.setAttrWater(attribute.getValue());
					continue;
				}

				if(currentElement.equals("lighting")){
					growingTips.setLighing("yes");
					continue;
				}

			}

			if (event.isCharacters()) {
				Characters characters = event.asCharacters();
				String elementText = characters.getData();

				if(currentElement.equals("name")){
					flower.setName(elementText);
					continue;
				}

				if(currentElement.equals("soil")){
					flower.setSoil(elementText);
					continue;
				}

				if(currentElement.equals("origin")){
					flower.setOrigin(elementText);
					continue;
				}

				if(currentElement.equals("stemColour")){
					visualParameters.setStemColour(elementText);
					continue;
				}

				if(currentElement.equals("leafColour")){
					visualParameters.setLeafColour(elementText);
					continue;
				}

				if(currentElement.equals("aveLenFlower")){
					visualParameters.setAveLen(Integer.parseInt(elementText));
					continue;
				}

				if(currentElement.equals("tempreture")){
					growingTips.setTempreture(Integer.parseInt(elementText));
					continue;
				}

				if(currentElement.equals("watering")){
					growingTips.setMeasure(Integer.parseInt(elementText));
					continue;
				}

				if(currentElement.equals("multiplying")){
					flower.setMultiplying(elementText);
					continue;
				}

			}

			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				String localName = endElement.getName().getLocalPart();

				if(localName.equals("flower")){
					flowers.getFlower().add(flower);
				}

				if(localName.equals("visualParameters")){
					flower.setVp(Collections.singletonList(visualParameters));
				}

				if(localName.equals("growingTips")){
					flower.setGt(Collections.singletonList(growingTips));
				}

			}

		}
		reader.close();
	}

	public static Comparator<Flower> compByMulti = new Comparator<Flower>() {
		@Override
		public int compare(Flower o1, Flower o2) {
			return o1.getMultiplying().compareTo(o2.getMultiplying());
		}
	};

	public static final void sortByTemp(Flowers flowers) {
		Collections.sort(flowers.getFlower(), compByMulti);
	}


}