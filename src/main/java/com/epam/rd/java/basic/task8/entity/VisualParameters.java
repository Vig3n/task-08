package com.epam.rd.java.basic.task8.entity;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private int aveLen;
    private String lenAttr;


    public VisualParameters(String stemColour, String leafColour, int aveLen) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLen = aveLen;
    }

    public VisualParameters(){

    }

    public String getLenAttr() {
        return lenAttr;
    }

    public void setLenAttr(String lenAttr) {
        this.lenAttr = lenAttr;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLen(int aveLen) {
        this.aveLen = aveLen;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public int getAveLen() {
        return aveLen;
    }

    @Override
    public String toString() {
        return "VisualParameters{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLen=" + aveLen +
                ", lenAttr='" + lenAttr + '\'' +
                '}';
    }
}
